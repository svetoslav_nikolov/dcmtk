pushd "%~dp0.."

IF EXIST dcmtk_vs2015 (
    rd /s /q dcmtk_vs2015
)
mkdir dcmtk_vs2015
pushd dcmtk_vs2015

echo %CD%

cmake -DCMAKE_INSTALL_PREFIX=%~dp0\..\dcmtk_distro^
      -DDCMTK_WITH_TIFF=OFF^
      -DDCMTK_WITH_PNG=OFF^
      -DDCMTK_WITH_ICU=OFF^
      -DDCMTK_WITH_ZLIB=OFF^
      -DDCMTK_WITH_OPENSSL=OFF^
      -DDCMTK_WITH_ICONV=OFF^
      -DBUILD_SHARED_LIBS=ON^
      -DBUILD_APPS=ON^
      -G "Visual Studio 14 2015 Win64"^
      ../dcmtk

@ECHO ========================================================================
@ECHO  The Solution file is in  %~dp0\..\build_vs2015
@ECHO  Open it with Visual Studio.
@ECHO ========================================================================

popd
popd

PAUSE
